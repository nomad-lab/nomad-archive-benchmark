# nomad-archive-benchmark
This repository contains benchmark code and results for different NOMAD Archive
erialization formats.

## Installation
```sh
pip install -e .[dev]
```

## Running tests
The tests can be run through a CLI. Here are some of the common tasks:

```sh
# Run the tests. You can choose specific formats or specific tests.
nomad-archive-benchmark run --formats=json --tests=write

# Create plots from the test result files
nomad-archive-benchmark plot --show
``
