#
# Copyright The NOMAD Authors.
#
# This file is part of NOMAD. See https://nomad-lab.eu for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import click


class POPO(dict):
    """
    A dict subclass that uses attributes as key/value pairs.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError('No such attribute: ' + name)

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        if name in self:
            del self[name]
        else:
            raise AttributeError('No such attribute: ' + name)


@click.group(
    help=(
        "This is the entry point to nomad's command line interface CLI. "
        'It uses a sub-command structure similar to the git command.'
    )
)
@click.pass_context
def cli(ctx):
    pass


@cli.command()
@click.option('--formats', help='Formats to test.', required=True)
@click.option('--tests', help='Tests to run.', required=True)
@click.option('-n', help='Number of repetitions.', default=1, required=True)
def run(formats, tests, n):
    pass


@cli.command()
@click.option('--show', is_flag=True, help='Whether to show the plot')
def plot(show):
    plt.rcParams.update(
        {
            'text.usetex': True,
            'font.family': 'serif',
            'font.sans-serif': 'Computer Modern Serif',
            'axes.titlesize': 20,
            'axes.labelsize': 20,
            'lines.markersize': 9,
            'xtick.labelsize': 16,
            'ytick.labelsize': 16,
        }
    )
    figsize = (8, 6)
    fig, ax1 = plt.subplots(figsize=figsize)


def run_cli():
    return cli(obj=POPO())
