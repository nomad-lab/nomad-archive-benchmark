from typing import IO


class ArchiveHDF5:
    def __init__(self, file: str | IO):
        self._file: str | IO = file

    def __repr__(self):
        file_path: str = (
            f' ({self._buffer_or_path})'
            if isinstance(self._buffer_or_path, str)
            else ''
        )
        return f'ArchiveHDF5{file_path}'

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __getitem__(self, item):
        return self.read(item)

    def read(self, path: str | list = None):
        """
        Reads the data from the given path.

        This method navigates through the data structure based on the provided path.
        The path can be a string or a list. If it's a string, it's split into a list
        using '/' as the separator. Each element of the list is used to navigate
        through the data structure.

        If the path is None, it returns the root object.

        :param path: the path to the data to read
        :return: The data at the given path.
        """
        pass
