from abc import ABC, abstractmethod
from typing import IO


class Adapter(ABC):
    @abstractmethod
    def write(self, file: IO, data):
        pass

    @abstractmethod
    def read(self, file: IO):
        pass
